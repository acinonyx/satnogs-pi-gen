# SatNOGS pi-gen

satnogs-pi-gen is the tool to create the Raspbian SatNOGS Image.
It is based on [RPI-Distro/pi-gen](https://github.com/RPI-Distro/pi-gen).

Upstream README at [README.md](./README.md).

## Raspbian SatNOGS Image

The download link for the latest release of the Raspbian SatNOGS Image can be found at
[Releases](https://gitlab.com/librespacefoundation/satnogs/satnogs-pi-gen/-/releases).

## Development

see [Development Documentation](./docs/development.md)
